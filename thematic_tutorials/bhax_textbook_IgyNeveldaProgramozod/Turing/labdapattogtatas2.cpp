#include <iostream>
#include <stdlib.h>
#include <curses.h>
#include <unistd.h>

int main(void)
{
WINDOW *ablak;
ablak = initscr();
int indexX = 1,indexY=1 ,myX=0,myY=0,positionXright=0;
int positionXleft=0,positionYright=0,positionYleft=0;
nodelay(ablak, true);
getmaxyx(ablak,myY,myX);
myY*=2;
myX*=2;

for(;;)
{
	positionXleft = (positionXleft-1)%myX;
	positionXright = (positionXright+1)%myX;
	positionYleft = (positionYleft+-1)%myY;
	positionYright = (positionYright+1)%myY;
	clear();
	mvprintw(abs((positionYleft+(myY-positionYright)) / 2),abs((positionXleft+(myX-positionXright)) / 2), "O");
	refresh();
	usleep(100000);
}
return 0;
}

