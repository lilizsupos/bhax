#include <stdio.h>

int main()
{

	int a =2;
	int b =1;

	printf("a=%d,b=%d\n",a,b);
//segédváltozóval	
	int c = a;
	a= b;
	b=c;
	printf("a=%d,b=%d\n",a,b);
//segédváltozó nélkül
	b=b-a;
	a=a+b;
	b=a-b;
	printf("a=%d,b=%d\n",a,b);
return 0;
}
