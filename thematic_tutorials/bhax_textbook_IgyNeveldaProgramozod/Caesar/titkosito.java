
import java.io.InputStream;
import java.io.OutputStream;

public class main
{
   

    public static void titkosito(String kulcs, InputStream in, OutputStream out) throws java.io.IOException
    {
        
        byte[] kulcs = kulcs.getBytes();
        byte[] inputBuffer = new byte[256];
        int kulcsIndex = 0;
        int olvasottBajtok = 0;
        
        while((olvasottBajtok = in.read(inputBuffer)) != -1)
        {
            for(int i=0; i<olvasottBajtok; i++)
            {
                inputBuffer[i] = (byte)(inputBuffer[i] ^ kulcs[kulcsIndex]);
                kulcsIndex = (kulcsIndex + 1) % kulcs.length();
            }
            
            out.write(inputBuffer, 0, olvasottBajtok);
        }
        
    
        
        
        
    }

    public static void main(String[] args)
    {
    
    
        if(args[0] != "")
        {
            try
            {
                titkosito(args[0], System.in, System.out);
            }
            catch(java.io.IOException e)
            {
                e.printStackTrace();
            }
        }
        else
        {
            System.out.println("kérem irja be a kulcsot!");
            System.out.println("java main <kulcs>");
        }
        
    }
}

